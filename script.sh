#!/bin/bash
mode=random
#Mode list:
#[zero]:Fill disk with lots of 0 [device:/dev/zero]
#[random]:Fill disk with lost of random nums [devices:/dev/urandom]
#[hybirdfill]:Fill disk with 0 first,and fill disk with random nums.
#[encrypt_rand]:Encrypt disk first,and fill disk in Hybird Mode,than fill with zero.


pass=3
#In normal situation,3-Pass wipe is a safe and high performance choice.
#I don't suggest you set the wipe pass more than 5,may damage your disk.

disk=/dev/null

function zerofill() {
    count=1
        for((i=1;i<=$1;i++));
        do

        dd if=/dev/zero of=$disk bs=1M oflag=direct status=progress
        echo "[<] Pass $count end. $1 count[s] in total."
        echo "[*] Script will go ahead after a sleep of 10 seconds."
        sleep 10
        let count+=1

        done
        echo "[<] Everyting done.Re install your OS please."


}

function randfill(){
     count=1
        for((i=1;i<=$1;i++));
        do
        
        
        dd if=/dev/urandom of=$disk bs=1M oflag=direct status=progress
        echo "[<] Pass $count end. $1 count[s] in total."
        echo "[*] Script will go ahead after a sleep of 10 seconds."
        sleep 10
        
        let count+=1

        done
        echo "[<] Everyting done.Re install your OS please."
}

function hybirdfill(){
    count=1
        for((i=1;i<=$1;i++));
        do

        dd if=/dev/zero of=$disk bs=1M oflag=direct status=progress
        echo "[<] Zero filling end."
        sleep 3
        dd if=/dev/urandom of=$disk bs=1M oflag=direct status=progress
        echo "[<] Random filling end."
        echo "[<] Pass $count end. $1 count[s] in total."
        echo "[*] Script will go ahead after a sleep of 10 seconds."
        sleep 10
        let count+=1

        done
        echo "[<] Everyting done.Re install your OS please."

}

function encrypt_rand(){
    count=1
        cryptsetup open --type=plain $disk wipe_disk
        for((i=1;i<=$1;i++));
        do

        dd if=/dev/zero of=/dev/mapper/wipe_disk bs=1M oflag=direct status=progress
        sleep 3
        dd if=/dev/urandom of=/dev/mapper/wipe_disk bs=1M oflag=direct status=progress
        echo "[<] Pass $count end. $1 count[s] in total."
        echo "[*] Script will go ahead after a sleep of 10 seconds."
        sleep 10
        let count+=1
        
        done
        echo "[<] Now make disk "$disk" blank."
        dd if=/dev/zero of=$disk bs=1M oflag=direct status=progress
        echo "[<] Everyting done.Re install your OS please."
}

echo "[*] Disk Ruby - Disk Safe Eraser"
echo "[*] By djkaguya(djkaguya@protonmail.ch)"

sleep 3

echo "[*] Script working mode:$mode"

sleep 2

if [ $UID -ne 0 ]; then
    echo "[!] Superuser privileges are required to run this script."
    echo "[#] To run this script in root user, excute \"sudo $0\""
    exit 1
fi

case $mode in
        zero)
                echo "[>] Action "Zero Fill" for $pass pass[es] on process"
                zerofill $pass $goal
                ;;
        
        random)
                echo "[>] Action "Random Fill" for $pass pass[es] on process"
                randfill $pass $goal
                ;;        
        hybirdfill)
                echo "[>] Action "Hybird Fill" for $pass pass[es] on process"
                hybirdfill $pass $goal
                ;;
        
        encrypt_rand)
                echo "[>] Action "Encrypt Disk with Hybird Fill" for $pass[es] on process"
                encrypt_rand $pass $goal
                ;;

        *)
                echo "[X] ERROR:No mode config for excute!"
                echo "[!] Script will exit in 5 seconds..."
                sleep 5
                exit 1
                ;;

esac