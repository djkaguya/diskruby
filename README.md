# DiskRuby 使用者文档

## 0 介绍
这是一个相对简（ruo）洁（zhi）的硬盘抹掉工具，依赖于GNU/Linux环境运行。
## 1 从零使用
**本程序的运行需要一个Live 运行环境支撑，请自行选择发行版或者使用本文中推荐的Arch Linux演示环境。**
由于可能存在的被怀疑捆绑其他恶意代码，我**不提供**预封装镜像。
---
### 1x01 建立环境
+ 建议使用的Live环境：Arch Linux
+ 到[清华大学开源镜像站](https://mirrors.tuna.tsinghua.edu.cn/)下载或者前往[Arch Linux列出的镜像列表](https://www.archlinux.org/download/)取得镜像。
+ 使用[Win32DiskImager](https://sourceforge.net/projects/win32diskimager/)或者[Balena Etcher](https://www.balena.io/etcher/)进行镜像克隆到U盘。
+ 从U盘启动Arch Linux。
***
### 1x02 获取DiskRuby
+ 通过执行`Ping gitlab.com`确认设备与Gitlab可以建立联接。
+ 若连接可靠，执行`wget https://gitlab.com/djkaguya/diskruby/raw/master/script.sh -O diskruby`来下载DiskRuby。
+ 执行`chmod +x diskruby.sh`，给予DiskRuby可执行权限。


**不要**直接运行DiskRuby，否则预先设置的保护变量会使系统报错。
***

### 1x03 调整DiskRuby
+ 打开文字编辑器，如`nano diskruby`，修改其变量`mode=`，`pass=`及`disk=`后内容。
+ 其中，`mode`有三种可用模式：
1.zero：直接向对象磁盘中填充0
2.random：直接向对象磁盘填充随机数
3.hybirdfill：混合两种填写方法（其中`zero`在先，`random`在后）。
4.enctypt_rand：通过`加密对象磁盘`来达到**预先填充**效果。
+ `pass`建议设置量：3-5


**过多次数的覆盖写入可能减短硬盘寿命，请酌情修改。**
+ `disk`为Linux系统指派的`硬件设备号`，你可以通过`ls /dev/sd*`来确定需要修改的被填充硬盘


**如果需要通过硬盘型号等信息确认，可执行`fdisk -l`并忽略类似`Disk /dev/loop`的信息**
+ 确认修改完成后，按下`Ctrl + O`并回车，保存文件退出Nano。
***
### 1x04 执行Diskruby
+ 直接输入`./diskruby`，系统开始自动执行。




